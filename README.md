# LHEventAnalysis

Simple framework to perform event selection analysis.
I created it to use as an adapter for the pylhe module, but it can be easily extended 
to other libraries, since the core classes do not rely on the pylhe module.

The most important class is the Analysis class, which can be found in [src/Analysis.py](src/Analysis.py). The latter is 
responsible to launch the event selection and updates the histograms defined by the user.

The event selection uses the cuts implemented by the user. A cut function must take an event as input and returns True 
if the event passed the cut, and False otherwise.

The Analysis class is only aware about the cut if the user register the cut with the @register_cut decorador as below. 

```python
  from src.Analysis import Analysis
  
  @Analysis.register_cut
  def user_cut(event):
    pass
```

The histograms must follow the Histogram interface defined in [src/Histogram.py](src/Histogram.py). This interface 
specifies a method called update_histogram, that the Analysis class uses to update all the user defined 
histograms. A concrete Histogram class can be found in the same file.

Simple usages of the core classes can be found in [examples/](examples). 