from src.Analysis import Analysis
from src.LHEventAdapter import LHEventAdapter
import vector


@Analysis.register_cut
def lepton_cuts(event: LHEventAdapter):
    """
    Cuts on the final state charged leptons.
    The lepton requirements are pT > 20 GeV and |eta| < 2.5.
    """
    return all(momentum.pt > 20 and abs(momentum.eta) < 2.5 for momentum in event.leptons_momentum)


@Analysis.register_cut
def jet_selection(event: LHEventAdapter):
    """Kinematical cuts on the jets."""
    return all(momentum.pt > 20 and abs(momentum.eta) < 5. for momentum in event.jets_momentum)


@Analysis.register_cut
def invariant_mass_jet_pair(event: LHEventAdapter):
    """Mjj > 100 GeV"""
    jets_total_momentum = sum(event.jets_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return jets_total_momentum.M > 100


@Analysis.register_cut
def jets_distance_cut(event: LHEventAdapter):
    """DeltaR(j, j) > 0.4"""
    jets_momentum = event.jets_momentum
    return jets_momentum[0].deltaR(jets_momentum[1]) >= 0.4
