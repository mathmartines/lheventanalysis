import vector
from src.HistogramFactory import HistogramFactory
from src.Histogram import HistogramList
from src.LHEventAdapter import LHEventAdapter
from src.SimulationFilesReader import LHESimulationsReader
from src.utilities import read_xsection
import examples.lnv.cuts
import yaml


def invariant_mass_observable(event: LHEventAdapter):
    """Evaluates the invariant mass of the charged lepton pair."""
    total_momentum = sum(event.leptons_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return total_momentum.M


def leading_lepton_pt(event: LHEventAdapter):
    """Evaluates the leading lepton transverse momenta"""
    leptons_pt = [momentum.pt for momentum in event.leptons_momentum]
    return max(leptons_pt)


def subleading_lepton_pt(event: LHEventAdapter):
    """
    Evaluates the leading lepton transverse momenta.
    Here we are assuming that there is only two leptons in the event.
    """
    leptons_pt = [momentum.pt for momentum in event.leptons_momentum]
    return min(leptons_pt)


def ssww_production_analysis():
    # defining the histograms
    hist_invariant_mass = HistogramList(
        observable=invariant_mass_observable,
        bin_edges=[0, 250, 500, 750, 1000, 100000],
        hist_name="Invariant mass of the charged leptons"
    )
    hist_leading_lepton_pt = HistogramList(
        observable=leading_lepton_pt,
        bin_edges=[0, 200, 300, 400, 600, 100000],
        hist_name="Leading lepton pT"
    )
    hist_subleading_lepton_pt = HistogramList(
        observable=subleading_lepton_pt,
        bin_edges=[0, 200, 300, 400, 500, 100000],
        hist_name="Sub-leading lepton pT"
    )

    # creating the histogram factory
    hist_factory = HistogramFactory.fromlist([hist_invariant_mass, hist_leading_lepton_pt, hist_subleading_lepton_pt])

    # list with all the lhe files to analyse
    root_folder = "/home/martines/work/MG5_aMC_v3_1_1/LNV/Background/SM_first"
    complement = "Events/run_01/pythia8_decay.lhe"
    xsection_complement = "Events/run_01/run_01_tag_1_banner.txt"
    lhe_files = [f"{root_folder}/bin_{index}/{complement}" for index in range(1, 7)]
    xsections = [
        2 * 1.111202e-01 * 1.111202e-01 * 139 * 1000 * read_xsection(f"{root_folder}/bin_{index}/{xsection_complement}")
        for index in range(1, 7)
    ]
    print(xsections)

    # launching the analysis on the event files
    simulations = LHESimulationsReader(histogram_factory=hist_factory, event_files=lhe_files, xsections=xsections)
    simulations.launch_analysis_on_simulations()

    # print the results for each histogram
    print("INFO: Results from the analysis")
    for histogram in simulations.get_histograms().values():
        print(histogram)
        with open(f"LNV/SM_{histogram.hist_name}.yaml", "w") as file_:
            yaml.dump({"SM": histogram}, file_)


if __name__ == "__main__":
    ssww_production_analysis()
