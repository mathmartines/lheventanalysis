from src.Analysis import Analysis
from src.Histogram import HistogramList
from src.LHEventAdapter import LHEventAdapter
import pylhe
import vector


@Analysis.register_cut
def number_of_leptons(event: LHEventAdapter):
    """Number of leptons must be greater than 1."""
    return len(event.leptons) > 1


@Analysis.register_cut
def leptons_cut(event: LHEventAdapter):
    """
    Cuts on the final state charged leptons.
    The lepton requirements are pT > 20 GeV and |eta| < 2.5.
    """
    return all(momentum.pt > 20 and abs(momentum.eta) < 2.5 for momentum in event.leptons_momentum)


@Analysis.register_cut
def number_of_jets(event: LHEventAdapter):
    """Number of jets has to be at least two"""
    return len(event.jets) > 1


def invariant_mass_observable(event: LHEventAdapter):
    """Evaluates the invariant mass of the charged lepton pair."""
    total_momentum = sum(event.leptons_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return total_momentum.M


def leptons_pt(event: LHEventAdapter):
    """Evaluates the transverse momentum of the charged lepton pair."""
    total_momentum = sum(event.leptons_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return total_momentum.pt


if __name__ == "__main__":
    # defining the histograms
    hist_invariant_mass = HistogramList(
        observable=invariant_mass_observable,
        bin_edges=[0, 200, 400, 600, 1000, 1000000],
        hist_name="Invariant mass of the charged leptons"
    )
    hist_leptons_pt = HistogramList(
        observable=leptons_pt,
        bin_edges=[0, 100, 300, 500, 1000, 1500, 2000, 100000],
        hist_name="Transverse momentum of the lepton pair"
    )

    # setting up the analysis
    path_to_lhe = "/home/martines/work/MG5_aMC_v3_1_1/LNV/Background/SM_first/bin_1/Events/run_01/pythia8_decay.lhe"
    analysis = Analysis((LHEventAdapter(event) for event in pylhe.read_lhe(path_to_lhe)))

    # adding the histogram to be filled
    analysis.add_histogram(hist_invariant_mass)
    analysis.add_histogram(hist_leptons_pt)

    # launching the analysis
    analysis.select_events()

    # showing the results
    print(hist_invariant_mass)
    print(hist_leptons_pt)

    # cuts statistics
    analysis.display_cuts_stats()
