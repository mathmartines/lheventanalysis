from src.Analysis import Analysis
from src.Histogram import HistogramList
from src.HistogramFactory import HistogramFactory
from src.LHEventAdapter import LHEventAdapter
from src.SimulationFilesReader import LHESimulationsReader
from src.utilities import read_xsection
import vector


@Analysis.register_cut
def number_of_leptons(event: LHEventAdapter):
    """Number of leptons must be greater than 1."""
    return len(event.leptons) > 1


@Analysis.register_cut
def leptons_cut(event: LHEventAdapter):
    """
    Cuts on the final state charged leptons.
    The lepton requirements are pT > 20 GeV and |eta| < 2.5.
    """
    return all(momentum.pt > 20 and abs(momentum.eta) < 2.5 for momentum in event.leptons_momentum)


@Analysis.register_cut
def number_of_jets(event: LHEventAdapter):
    """Number of jets has to be at least two"""
    return len(event.jets) > 1


def invariant_mass_observable(event: LHEventAdapter):
    """Evaluates the invariant mass of the charged lepton pair."""
    total_momentum = sum(event.leptons_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return total_momentum.M


def leptons_pt(event: LHEventAdapter):
    """Evaluates the transverse momentum of the charged lepton pair."""
    total_momentum = sum(event.leptons_momentum, vector.MomentumObject4D(px=0, py=0, pz=0, e=0))
    return total_momentum.pt



if __name__ == "__main__":
    # defining the histograms
    hist_invariant_mass = HistogramList(
        observable=invariant_mass_observable,
        bin_edges=[0, 200, 400, 600, 1000, 1000000],
        hist_name="Invariant mass of the charged leptons"
    )
    hist_leptons_pt = HistogramList(
        observable=leptons_pt,
        bin_edges=[0, 100, 300, 500, 1000, 1500, 2000, 100000],
        hist_name="Transverse momentum of the lepton pair"
    )

    # creating the histogram factory
    hist_factory = HistogramFactory.fromlist([hist_invariant_mass, hist_leptons_pt])

    # list with all the lhe files to analyse
    root_folder = "/home/martines/work/MG5_aMC_v3_1_1/LNV/Background/SM_first"
    complement = "Events/run_01/pythia8_decay.lhe"
    lhe_files = [f"{root_folder}/bin_{index}/{complement}" for index in range(1, 7)]

    # reading the x-sections
    xsectons = [
        read_xsection(f"{root_folder}/bin_{index}/Events/run_01/run_01_tag_1_banner.txt") for index in range(1, 7)
    ]

    # using the SimulationLauncher concrete class
    simulations = LHESimulationsReader(histogram_factory=hist_factory, event_files=lhe_files, xsections=xsectons)

    # launching the analysis on the files
    simulations.launch_analysis_on_simulations()

    for histogram in simulations.get_histograms().values():
        print(histogram)
