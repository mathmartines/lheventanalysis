from typing import Callable
from src.Histogram import Histogram
from typing import List


class Analysis:
    """
    Class responsible to perform the event selection in an iterable of events.

    The user can register a function that represents a cut using the @Analysis.register_cut decorator.
    The cut function must return True if the event passed the cut and False otherwise.

    Histograms can be added by the method add_histogram. They must follow the interface Histogram, i.e.
    they must provide a method called update_histogram.

    To perform the event selection, one can call the method select_events.
    This method updates the histograms with the events that passed the cuts.
    """
    # stores the cuts to be applied in the analysis
    user_cuts = []

    def __init__(self, events):
        self.__events = list(events)
        self.__hist_list = list()
        self.__cuts_stats = dict()

    @classmethod
    def register_cut(cls, cut: Callable):
        """Adds a new cut to list of cuts that must be applied during the event selection."""
        cls.user_cuts.append(cut)
        print(f"INFO: Cut {cut.__name__} added to the analysis")
        return cut

    def remove_cut(self, cut_name: str):
        self.user_cuts.remove(eval(cut_name))

    def add_histogram(self, histogram: Histogram):
        if isinstance(histogram, Histogram):
            self.__hist_list.append(histogram)
        else:
            raise TypeError(f"the {histogram.__name__} is not of the type Histogram")

    def add_histograms(self, histograms: List[Histogram]):
        for histogram in histograms:
            self.add_histogram(histogram)

    def validate_event(self, event):
        """Returns True if the event passes all the cuts and False otherwise."""
        return all(cut(event) for cut in self.user_cuts)

    def select_events(self):
        """
        Launches the event selection.
        It updates the histograms with the events that passed all the cuts.
        """
        index = 1
        for event in self.__events:
            if self.validate_event(event):
                self.update_histograms(event)
            if index % 1000 == 0:
                print(f"INFO: reached {index} events")
            index += 1
        for hist_ in self.__hist_list:
            print(hist_)

    def update_histograms(self, event):
        for histogram in self.__hist_list:
            histogram.update_histogram(event)

    def compute_cuts_stats(self):
        # initializing the counterts
        self.__cuts_stats = {cut.__name__: 0 for cut in self.user_cuts}
        for event in self.__events:
            for cut in self.user_cuts:
                if cut(event):
                    self.__cuts_stats[cut.__name__] += 1

    def display_cuts_stats(self):
        self.compute_cuts_stats()
        print("\nCuts statistics (events passed the cut / total number of events):")
        for cut_name, events_passed in self.__cuts_stats.items():
            print(f"{cut_name}: {events_passed}/{len(self.__events)}")

    @property
    def events_list(self):
        return self.__events

    @events_list.setter
    def events_list(self, events):
        self.__events = list(events)

    def get_histograms(self):
        return (histogram for histogram in self.__hist_list)