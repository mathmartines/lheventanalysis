import abc


class Event(abc.ABC):
    """
    Simple interface to provide easy acess to the final state particles
    """

    @property
    @abc.abstractmethod
    def leptons(self):
        pass

    @property
    @abc.abstractmethod
    def neutrinos(self):
        pass

    @property
    @abc.abstractmethod
    def jets(self):
        pass
