import abc
import collections.abc
from typing import Callable


class Histogram(abc.ABC):
    """
    Interface that the histograms must follow.
    The subclasses must implement the method update_histogram.
    """

    def __init__(self, histogram_name: str):
        self.hist_name = histogram_name

    @abc.abstractmethod
    def update_histogram(self, event):
        """Must provide a method to update the histogram."""
        pass

    @abc.abstractmethod
    def __copy__(self):
        """Must return a clone of the same histogram but uniflled."""


class HistogramList(Histogram, list):
    """
    Simple implementation of the Histogram interface that behaves like a python list.
    This can be obviously improved by using external libraries such as numpy.
    I decided to do this implementation by myself just as an exercise.

    In case someone uses, there are some details below:

    The values at each bin can be acess using hist[index].

    Also, the computation of the observable is delegated to a Callable function that must return a
    float or an integer.
    This function must be passed in the constructor, and it can not be updated at runtime.

    The following operations are supported by the class:

    1. imul - rescales all the bins by some factor passed;
    2. add - sums the entries of two histograms;
    3. iadd - adds the entries of a histogram to the left hand side histogram.
    """

    def __init__(self, observable: Callable, bin_edges: collections.abc.Sequence, hist_name: str):
        self.__observable = observable
        self.bin_edges = bin_edges
        list.__init__(self, [0] * (len(bin_edges) - 1))
        Histogram.__init__(self, hist_name)

    def update_histogram(self, event):
        try:
            # find the bin index and updates the histogram
            bin_index = self.find_bin_index(self.__observable(event))
            self[bin_index] += 1

        except IndexError:
            print(f"Event with observable value {self.__observable(event)} is out of the histogram bounds")

    def find_bin_index(self, observable_value: float, index: int = 0):
        if index + 1 == len(self.bin_edges):
            raise IndexError
        elif self.bin_edges[index] <= observable_value < self.bin_edges[index + 1]:
            return index
        else:
            return self.find_bin_index(observable_value, index + 1)

    def __imul__(self, factor):
        """Rescale all entries of the histogram"""
        for index in range(len(self)):
            self[index] *= factor
        return self

    def __add__(self, other):
        """Returns a new histogram object"""
        resulting_hist = HistogramList(self.__observable, self.bin_edges, self.hist_name)
        for index in range(len(self)):
            resulting_hist[index] = self[index] + other[index]
        return resulting_hist

    def __mul__(self, factor):
        """Returns a new histogram object"""
        resulting_hist = HistogramList(self.__observable, self.bin_edges, self.hist_name)
        for index in range(len(self)):
            resulting_hist[index] = self[index] * factor
        return resulting_hist

    def __iadd__(self, other):
        for index in range(len(self)):
            self[index] += other[index]
        return self

    def __repr__(self):
        return f"{self.hist_name} - {list(self)}"

    def __copy__(self):
        return self.__class__(self.__observable, self.bin_edges, self.hist_name)
