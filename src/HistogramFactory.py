from src.Histogram import Histogram
from typing import List
import copy


class HistogramFactory:
    """
    Responsible to create the histograms defined by the user.
    """

    def __init__(self):
        # dictionary to store references to the Histogram classes.
        # The key must be the Histogram class name, while the value must be a reference to the class
        self.__hist_clones = dict()

    @classmethod
    def fromlist(cls, histograms: List[Histogram]):
        """Alternative constructor: creates a histogram factory from a list of histogram instances"""
        histogram_factory = cls()
        histogram_factory.__hist_clones = {histogram.hist_name: histogram for histogram in histograms}
        return histogram_factory

    def create_histogram(self, histogram_name: str):
        """Returns an empty clone of the histogram that has the name given in the argument."""
        return copy.copy(self.__hist_clones[histogram_name])

    def add_histogram(self, histogram: Histogram):
        """
        Adds a new histogram into the factory.
        :param histogram: a histogram that we'll clone to create histograms when the user asks for a Histogram
        with the same name.
        """
        self.__hist_clones[histogram.hist_name] = histogram

    def generate_histograms(self):
        return (self.create_histogram(hist_name) for hist_name in self.__hist_clones)
