import pylhe
import vector
from src.Event import Event


class LHEventAdapter(Event):
    """
    Implementation of the Event interface for the pylhe.LHEvent class
    """

    def __init__(self, lhe_events: pylhe.LHEEvent):
        self.__event = lhe_events

    def get_particles_by_id(self, pid_numbers: list):
        return [particle for particle in
                self.__event.particles if particle.status == 1 and abs(particle.id) in pid_numbers]

    def get_particles_momentum_by_id(self, pid_number: list):
        return [vector.MomentumObject4D(**{comp: getattr(particle, comp) for comp in "px py pz e".split()})
                for particle in self.get_particles_by_id(pid_number)]

    @property
    def leptons(self):
        return self.get_particles_by_id([11, 13])

    @property
    def neutrinos(self):
        return self.get_particles_by_id([12, 14])

    @property
    def jets(self):
        return self.get_particles_by_id(list(range(1, 6)))

    @property
    def leptons_momentum(self):
        return self.get_particles_momentum_by_id([11, 13])

    @property
    def neutrinos_momentum(self):
        return self.get_particles_momentum_by_id([12, 14])

    @property
    def jets_momentum(self):
        return self.get_particles_momentum_by_id(list(range(1, 6)))
