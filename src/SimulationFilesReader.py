from abc import ABC, abstractmethod
from typing import List
import pylhe
from src.HistogramFactory import HistogramFactory
from src.Analysis import Analysis
from src.LHEventAdapter import LHEventAdapter


class SimulationFilesReader(ABC):
    """
    Launches the analysis on a set of simulation files that represents the same simulation, but
    each file contais a different portion of the phase-space.
    """

    def __init__(self, histogram_factory: HistogramFactory, event_files: List[str], xsections: List[float]):
        self.event_files = event_files
        self.xsections = xsections
        self.hist_factory = histogram_factory
        # responsible to store the final histogram.
        # The final histogram is the sum of all the histograms from different events file
        self.final_histograms = {}

    def launch_analysis_on_simulations(self):
        """Launches the analysis on each of the simulations files"""
        for event_file, xsection in zip(self.event_files, self.xsections):
            print(f"INFO: launching analysis on file {event_file}")
            events = self.read_events(event_file)
            analysis = Analysis(events)
            analysis.add_histograms(self.hist_factory.generate_histograms())
            analysis.select_events()
            self.update_final_histograms(analysis, xsection, len(events))

    @abstractmethod
    def read_events(self, event_file):
        """It must provide a way to read events from a file"""
        pass

    def update_final_histograms(self, analysis: Analysis, xsection: float, number_of_events: int):
        """Updates the histograms"""
        weight = xsection / number_of_events
        for histogram in analysis.get_histograms():
            if histogram.hist_name in self.final_histograms:
                self.final_histograms[histogram.hist_name] += histogram * weight
            else:
                self.final_histograms[histogram.hist_name] = histogram * weight

    def get_histograms(self):
        return self.final_histograms


class LHESimulationsReader(SimulationFilesReader):
    """Class to read events from a .lhe file"""
    def read_events(self, event_file):
        return [LHEventAdapter(event) for event in pylhe.read_lhe(event_file)]
