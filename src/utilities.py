def read_xsection(filename):
    """Reads the cross-sections"""
    try:
        # open file
        file_ = open(filename, "r")
        # loop over the lines
        for line in file_:
            if "Integrated weight" in line:
                _, xsection = line.split(":")
                return float(xsection)
    except FileNotFoundError:
        print("INFO: file %s not found" % filename)
        return None
